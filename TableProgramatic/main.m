//
//  main.m
//  TableProgramatic
//
//  Created by Akhmad Harry Susanto on 2019/11/24.
//  Copyright © 2019 Akhmad Harry Susanto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
