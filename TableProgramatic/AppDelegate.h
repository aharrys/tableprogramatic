//
//  AppDelegate.h
//  TableProgramatic
//
//  Created by Akhmad Harry Susanto on 2019/11/24.
//  Copyright © 2019 Akhmad Harry Susanto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

