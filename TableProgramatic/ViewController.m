//
//  ViewController.m
//  TableProgramatic
//
//  Created by Akhmad Harry Susanto on 2019/11/24.
//  Copyright © 2019 Akhmad Harry Susanto. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) UITableView *table;
@property (strong, nonatomic) NSArray *content;

@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    //first one
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(45, 30, 200, 40)];
    tf.textColor = [UIColor colorWithRed:0/256.0 green:84/256.0 blue:129/256.0 alpha:1.0];
    tf.font = [UIFont fontWithName:@"Helvetica-Bold" size:25];
    tf.backgroundColor=[UIColor whiteColor];
    tf.text=@"Hello World";
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(100, 50, 400, 400)];
    [view addSubview:tf];
    
    [self configuretableView];
    [self.view addSubview:view];
    self.content = @[@"Sunday", @"Monday", @"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday"];
}

- (void) configuretableView
{
    CGFloat x = 0;
    CGFloat y = 200;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height - 50;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    self.table= [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
    self.table.delegate = self;
    self.table.dataSource = self;
    [self.view addSubview:self.table];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _content.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [self.table dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = [_content objectAtIndex:indexPath.row];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"titleof Cell %@", [_content objectAtIndex:indexPath.row]);
    NSString *message = [self.content objectAtIndex:indexPath.row];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"You Are Choose"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
    //Add Buttons
}


@end
